# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models,fields,api,_
from openerp.exceptions import Warning


class fleet_reg(models.Model):
    _name = 'fleet.reg'
    _rec_name = 'name'

    name  = fields.Char(string="Company Name")

    @api.model 
    def create(self,vals):
    	print "\n\n---self--",self
    	print "\n\n---create vals---",vals
    	if len(vals['name']) < 4:
    		raise Warning('Please, Enter more then 4 character')
    	return super(fleet_reg,self).create(vals) 
    	
    @api.multi
    def write(self,vals):
    	print "\n\n--write self--",self
    	print "\n\n--write vals--", vals
    	if len(vals['name']) < 4:
    		raise Warning('Please, Enter more then 4 character')
    	return super(fleet_reg,self).write(vals)


    @api.one
    def copy(self, default={'name':name}):
        print "\n ======= copy=========== self :",self,default
        data = super(fleet_reg, self).copy(default)
        print "\n ------- data------ :",data
        return data

    @api.multi
    def unlink(self):
    	print "\n\n --unlink call----",self
        res = self.env['fleet.spair_part'].search([('company_id','=',self.id)])
        print "\n\n---res---",res
        for each in res:
            print "\n\n --each---",each
            each.unlink()
    	return super(fleet_reg,self).unlink()

