# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models,fields,api,_
from openerp.exceptions import Warning


class fleet_spair_parts(models.Model):
    _name = 'fleet.spair_part'
    _rec_name = 'spairname'

    image= fields.Binary("Image",help="This field holds the image used as image for our customers, limited to 1024x1024px.")
    spairname = fields.Char(string = "Parts Name")
    discription = fields.Text("Discription",size=15, store=True)
    price = fields.Float(string="Price", default="10")
    company_id = fields.Many2one('fleet.reg','Company') 

    # parts_id = fields.Many2one('fleet.spair.part.name','Parts Name', invible = True)




    @api.multi
    def chang_part(self):  
        view_id = self.env['ir.model.data'].get_object_reference('fleet_mgt','chang_spairpart_price_view')
        if view_id:
            return{
            'name' : 'Parts Name',
            'type':'ir.actions.act_window',
            'res_model':'chang.spairpart.price',
            'view_id':view_id[1],
            'view_type':'form',
            'view_mode':'form',
            'target': 'new',
            
            }

    @api.model
    def create (self,vals):
        print "\n\n--create call--", vals
        print"\n\n --self---",self
        if not vals.get('price'):
            raise Warning('please enter price first.!')
        res = super(fleet_spair_parts,self).create(vals)
        print "\n\n --res---",res
        return res

    @api.multi
    def write(self,vals):
        print "\n\n-write call---",self
        print "\n\n --write vals---",vals
        return super(fleet_spair_parts,self).write(vals)


class fleet_spair_parts_name(models.Model):
    _name = 'fleet.spair.part.name'

    name = fields.Char('Parts Name')


class fleet_order(models.Model):
    _name = 'fleet.order'
    _rec_name ='partner_id'

    @api.one
    @api.depends('order_ids')
    def get_total(self):
        for each in self.order_ids:
            self.amount_total += each.subtotal

    charg   = fields.Integer(string = 'Extra Charges')
    amount_total = fields.Float(string = "Total",compute='get_total',store=True)


    @api.one
    def do_done(self):
        print "\n\n ---do_done----",self,self.status,self.partner_id
        self.status = 'done'

    @api.one 
    def do_cancel(self):
        print "\n\n ---do cancel---",self,self.status
        self.status = 'cancel'
   
    @api.multi
    def chang_name(self):
        print "\n\n --change_name---",self
        view_id = self.env['ir.model.data'].get_object_reference('fleet_mgt','order_cust_chang_name_view')
        if view_id:
            return{
            'name':'Customer',
            'type':'ir.actions.act_window',
            'res_model':'order.cust.chang.name',
            'view_id':view_id[1],
            'view_type':'form',
            'view_mode':'form',
            'target': 'new',
            'context':{'default_partner_id':self.partner_id.id}
            }

    @api.onchange('partner_id')
    def onchange_partner_id(self):
        if self.partner_id:
            self.mobile_no = self.partner_id.mobile

    partner_id = fields.Many2one('res.partner',string="Customer")
    mobile_no = fields.Char(string="Mobile No")
    order_ids = fields.One2many('fleet.order.line','order_id',string="Order Line")
    status = fields.Selection([('draft','Draft'),('done','Done'),('cancel','Cancel')],string="Status",default='draft')


class fleet_order_line(models.Model):
    _name = 'fleet.order.line'

    @api.multi
    @api.depends('qty','unitprice')
    def compute_subtotal(self):
        for each_line in self:
            if each_line.qty and each_line.unitprice:
                each_line.subtotal = (each_line.qty * each_line.unitprice)
                # self.amount_total= (each_line.qty * each_line.unitprice)

    order_id = fields.Many2one('fleet.order', string = 'Order')
    product_id = fields.Many2one('product.product',string="Product")
    qty = fields.Float('Qty')
    unitprice = fields.Float(string="Unit Price")
    subtotal = fields.Float(string="Sub Total",compute='compute_subtotal',store=True)
    
    


class fleet_service(models.Model):
    _name = 'fleet.service'
    _rec_name = 'partner_id'

    @api.onchange('partner_id')
    def onchange_partner(self):
        if self.partner_id:
            print "\n\n partner",self.partner_id
            self.account_receivable_id = self.partner_id.property_account_receivable.id
            self.account_payable_id = self.partner_id.property_account_payable.id


    @api.one 
    def button_dummy(self):
        return True



    @api.multi
    @api.depends('service_ids.price','charge')
    def compute_amount_total(self):
        amount_total = 0 
        for each_service_line in self.service_ids:
            if each_service_line.price:
                amount_total += each_service_line.price
        self.amount_total = amount_total + self.charge
        if self.amount_total < 0:
            raise Warning('please enter amount total greater than 0.')

    partner_id = fields.Many2one('res.partner',string="Customer")
    account_receivable_id = fields.Many2one('account.account',string="Account Receivable")
    account_payable_id = fields.Many2one('account.account',string="Account Payable")
    company_id = fields.Many2one('fleet.reg',string="Company")
    service_ids = fields.One2many('fleet.service.line','service_id', string="Service Line")
    charge = fields.Float(string="Service Charge")
    amount_total = fields.Float(string = "Total", compute='compute_amount_total',store=True)



class fleet_service_line(models.Model):
    _name = 'fleet.service.line'

    @api.onchange('spair_id')
    def onchange_spairpart_name(self):
        if self.spair_id:
            # print "\n\n---spair_id---", self.spair_id
            self.price = self.spair_id.price



    company_id = fields.Many2one('fleet.reg',string="Company")
    spair_id = fields.Many2one('fleet.spair_part', string = "Spair Part")
    # parts_id = fields.Many2one('fleet.spair.part.name', string = 'Parts Name')
    bike = fields.Char(string="Bike/Car Model")
    price = fields.Float(string="Price")
    service_id = fields.Many2one('fleet.service',string="Service ID")


class fleet_rent(models.Model):
    _name = 'fleet.rent'
    _rec_name = 'partner_id'

    @api.onchange('partner_id')
    def onchange_partner_id(self):
        address = ''
        if self.partner_id:
            street = self.partner_id.street or ' ' 
            street2 =  self.partner_id.street2 or ''
            city = self.partner_id.city or ''
            state_id = self.partner_id.state_id.name or ''
            country_id = self.partner_id.country_id.name or ''
            address = street+ ',' + street2+ ',' + city + ',' +  state_id + ',' + country_id

            print "\n\n---address---", address
            self.address = address


    rent = fields.Selection([('car','CAR'),('bike','BIKE')],'Rent Vehicle')
    partner_id = fields.Many2one('res.partner',string="Customer")
    image = fields.Binary("Image",help="This field holds the image used as image for our customers, limited to 1024x1024px.")
    city_id = fields.Many2one('fleet.city',string = 'City')
    rent_ids = fields.One2many('fleet.rent.order.line','rent_id',string="Fleet Line")
    cab_ids = fields.One2many('fleet.cab.fare.line','cab_id', string = "Cab Line")
    address = fields.Text(string="Address")
    


class fleet_rent_order_line(models.Model):
    _name = 'fleet.rent.order.line'

    company_id = fields.Many2one('fleet.reg',string="Company")
    model = fields.Char(string="Model")
    engine = fields.Char("Engine")
    color_id = fields.Many2one('fleet.cab.color',string = "Color")
    price = fields.Float(string="Rent")
    rent_id = fields.Many2one('fleet.rent',string = "Rent ID",invible=True)

class fleet_cab_color(models.Model):
    _name = 'fleet.cab.color'
    _rec_name = 'color'

    color = fields.Char(string = "Color")


class fleet_city_name(models.Model):
    _name = 'fleet.city'

    name = fields.Char (string= "City")

    

class fleet_cab_fare_line(models.Model):
    _name = 'fleet.cab.fare.line'
    

    company_id = fields.Many2one('fleet.reg',string="Company")
    basefare = fields.Char(string = "Base Fare")
    distancefare = fields.Char(string= "Distance Fare")
    ridetime = fields.Char(string = "Ride Time Fare")
    cancel = fields.Char(string = "Cancellation Fee")
    cab_id = fields.Many2one('fleet.rent', string = "Cab Id", invible=True)


class chang_cust_name(models.TransientModel):
    _name = 'order.cust.chang.name'

    partner_id = fields.Many2one('res.partner',string="Customer", store=True)
    replace_partner_id = fields.Many2one('res.partner',string="Chnage Customer")

    @api.one
    def chang_name(self):
        print "\n\n --self---",self,self.partner_id.id,self.replace_partner_id.id
        print "\n\n ---context----",self._context
        model_name = self._context.get('active_model')
        print "\n\n-- model_name",model_name
        model_id = self._context.get('active_id')
        print "\n\n-- model_id",model_id
        print "\n\n ---record_id---",record_id
        if self.replace_partner_id:
            record_id = self.env[model_name].search([('partner_id','=',self.partner_id.id)])
            for each in record_id:
                each.write({'partner_id':self.replace_partner_id.id})

        # record_id = self.env[model_name].browse(model_id)
        # print "\n\n-----record_id-----",record_id
        # record_id.write({'partner_id':self.partner_id.id})


class chang_parts(models.TransientModel):
    _name = 'chang.spairpart.price'


    parts_id = fields.Many2one('fleet.spair.part.name','Parts Name')
    price = fields.Float(string="Price")
    
    @api.one
    def chang_parts(self):
        print "\n\n ----self---",self,self.parts_id.id
        model_name = self._context.get('active_model')
        print "\n\n-- model_name",model_name
        model_id = self._context.get('active_id')
        print "\n\n-- model_id",model_id
        record_id = self.env[model_name].browse(model_id)
        print "\n\n-----record_id-----",record_id
        if self.price >=50:
            record_id.write({'parts_id':self.parts_id.id})
            record_id.write({'price':self.price})
        else:
            raise Warning('please enter amount greater than 50.')



class res_partner(models.Model):
    _inherit='res.partner'

    @api.multi
    def name_get(self):
        res = []
        if self._context.get('from_fleet'):
            print "\n\n--1---"
            for each in self:
                res.append((each.id, (each.name or '') + ' - ' + str(each.mobile or '')))
            return res
        if self._context.get('from_fleet_cust'):
            for each in self:
                # res.append((each.id,(each.title or '') + ' - ' + str(each.name or '')))
                res.append((each.id,(each.title.name or '') + '-' + (each.name or '') + ' - ' + str(each.email or '')))
            return res
        else:
            return super(res_partner,self).name_get()

    @api.model
    def name_search(self, name, args=None, operator='like', limit=100):
        args = args or []
        print "===self===",self
        print "===name===",name
        print "args===",args
        print "operator==",operator
        print "limit====",limit
        if self._context.get('from_fleet'):
            ids = self.search([('phone',operator,name)]).name_get()
            print "\n\n --ids---",ids
            return ids
        else:
            return super(res_partner, self).name_search(name=name, args=args, operator='like', limit=limit)