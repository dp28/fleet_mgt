# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models,fields,api,_


class  purchase_order(models.Model):
    _inherit = 'purchase.order'


    ex_charges = fields.Float(string = 'Extra Charges')

    # def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
    #     res = {}
    #     cur_obj=self.pool.get('res.currency')
    #     line_obj = self.pool['purchase.order.line']
    #     for order in self.browse(cr, uid, ids, context=context):
    #         res[order.id] = {
    #             'amount_untaxed': 0.0,
    #             'amount_tax': 0.0,
    #             'amount_total': 0.0,
    #             'ex_charges':0.0
    #         }
    #         val = val1 = charg_val =  0.0
    #         cur = order.pricelist_id.currency_id
    #         for line in order.order_line:
    #             val1 += line.price_subtotal
    #             charg_val = order.ex_charges
    #             print "\n\n --charg_val--",charg_val
    #             line_price = line_obj._calc_line_base_price(cr, uid, line,
    #                                                         context=context)
    #             line_qty = line_obj._calc_line_quantity(cr, uid, line,
    #                                                     context=context)
    #             for c in self.pool['account.tax'].compute_all(
    #                     cr, uid, line.taxes_id, line_price, line_qty,
    #                     line.product_id, order.partner_id)['taxes']:
    #                 val += c.get('amount', 0.0)
    #         res[order.id]['amount_tax']=cur_obj.round(cr, uid, cur, val)
    #         res[order.id]['amount_untaxed']=cur_obj.round(cr, uid, cur, val1)
    #         res[order.id]['ex_charges'] = cur_obj.round(cr, uid, cur, charg_val)
    #         res[order.id]['amount_total']=res[order.id]['amount_untaxed'] + res[order.id]['amount_tax'] + res[]
    #     return res




   